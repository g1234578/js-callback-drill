/*
    Problem 1:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Create a directory of random JSON files
        2. Delete those files simultaneously 
*/



const fs = require('fs');


function createRandomfiles(callback){

    fs.mkdir("JSONRandomFiles", {recursive:true},(error) => {
        if(error){
            console.log(error);
            return;
        } 
    })

    console.log("Directory created successfully");

    for(let index = 0;index<=10;index++){
        fs.writeFileSync(`./JSONRandomFiles/file${index}.json`,JSON.stringify(`file ${index}`),(error)=>{
            if(error){
                console.log(error);
                return;
            }
            
        });
        console.log("File created successfully" + index); 
    }
    callback();
}

function deleteRandomFiles(){

    for(let index=0;index<=10;index++){
        fs.unlink(`./JSONRandomFiles/file${index}.json`,(error)=>{
            if(error){
                console.log(error);
                return;
            }
            console.log("Sucessfully Deleted Files" + index);
        })
        // console.log("Sucessfully Deleted Files" + index);
    }
}

module.exports = {createRandomfiles,deleteRandomFiles};


