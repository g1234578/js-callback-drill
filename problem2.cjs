/*
    Problem 2:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Read the given file lipsum.txt
        2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
        3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
        4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
        5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
*/

const fs = require("fs");

function readFileFunction(filename, callback) {
  fs.readFile(filename, "utf8", (error, data) => {
    if (error) {
      console.log(error);
      return;
    } else {
      callback(data);
    }
  });
}

function writeFile(content, filename, callback) {
  fs.writeFile(filename, content, (error) => {
    if (error) {
      console.log(error);
      return;
    } else {
      callback(filename);
    }
  });
}

function appendFile(content, filename, callback) {
  fs.appendFile(filename, content, (error) => {
    if (error) {
      console.log(error);
      return;
    } else {
      callback(filename);
    }
  });
}

function deleteFile(filename, callback) {
  fs.unlink(filename, (err) => {
    if (err) {
      console.error(err);
      return;
    } else {
      callback(filename);
    }
  });
}

function callbackPerformOperations() {
  readFileFunction("test/lipsum_1.txt", (data) => {
    console.log("Readed Successfully" + "lipsum_1.txt");
    let uppercaseData = data.toUpperCase();
    writeFile(uppercaseData, "upperCaseData.txt", (filename) => {
      console.log("Written Sucessfully" +" " +filename);

      writeFile(filename + "\n", "fileName.txt", (filename) => {
        console.log("Written Successfully", filename);
        readFileFunction("upperCaseData.txt", (data) => {
          console.log("Readed Successfully" + "upperCaseData.txt");
          let lowerCaseData = data.toLowerCase();
          let paragraph = lowerCaseData.split("\n\n");
          let sentences = "";
          paragraph.forEach((para) => {
            let eachPara = para.trim();
            let sentenceData = eachPara.split(".");
            sentenceData.forEach((sentence) => {
              let sentenceContent = sentence.trim();
              if (sentenceContent != "") {
                sentenceContent += ".\n";
                sentences += sentenceContent;
              }
            });
          });
          writeFile(sentences, "sentences.txt", (filename) => {
            console.log("Written SuccessFully" + " " + filename);
            appendFile("sentences.txt\n", "fileName.txt", (filename) => {
              console.log("Appended Successfully" + filename);
              readFileFunction("sentences.txt", (data) => {
                console.log("File readed successfully" + " " + filename);
                let sortedSentences = data.split("\n");
                sortedSentences.sort();
                let totalSortedSentence = "";
                sortedSentences.forEach((sentence) => {
                  if (sentence != "") {
                    sentence += ".\n";
                    totalSortedSentence += sentence;
                  }
                });
                writeFile(
                  totalSortedSentence,
                  "sortedSentences.txt",
                  (filename) => {
                    console.log("Written successfully" + " " + filename);
                    appendFile(
                      "sortedSentences.txt\n",
                      "fileName.txt",
                      (filename) => {
                        console.log("Appended successfully" + " " + filename);
                        readFileFunction("fileName.txt", (data) => {
                          let filesNames = data.split("\n");
                          filesNames.forEach((file) => {
                            if (file !== "") {
                              deleteFile(file, (filename) => {
                                console.log("File is deleted" + " " + filename);
                              });
                            }
                          });
                        });
                      }
                    );
                  }
                );
              });
            });
          });
        });
      });
    });
  });
}

module.exports = callbackPerformOperations;
