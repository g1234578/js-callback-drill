const createRandomfiles = require('../problem1.cjs').createRandomfiles;
const deleteRandomFiles = require('../problem1.cjs').deleteRandomFiles;

try{
    createRandomfiles(()=>
        deleteRandomFiles()
    );
}
catch(error){
    console.log(error);
}